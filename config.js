module.exports = {
  serverAddress: 'http://192.168.199.12:8889/epm-taglib-system/model',
  commonVariables: {
    tenantId: 'system_tenant',
    loginId: 'admin',
    appId: 'epm_custgroup',
  },
  numOfPages: 10,
  port: 5998,
};
