const fs = require('fs');
const dimTagData = JSON.parse(fs.readFileSync('data/dimTagData.json'));
const QueryResult = require('./QueryResult');
const {
  GraphQLObjectType,
  GraphQLList,
  GraphQLInt,
  GraphQLString,
  GraphQLInputObjectType,
} = require('graphql');

const DimTagDataType = new GraphQLObjectType({
  name: 'DimTagDataType',
  fields: {
    code: {type: GraphQLString},
    name: {type: GraphQLString},
    parentCode: {type: GraphQLString},
    ord: {type: GraphQLString},
  },
});

const DimTagDataInput = new GraphQLInputObjectType({
  name: 'DimTagDataInput',
  fields: {
    tenantId: {type: GraphQLString},
    loginId: {type: GraphQLString},
    appId: {type: GraphQLString},
    tagId: {type: GraphQLString},
    dimDataCodeValues: {type: GraphQLString},
    dimDataParentCodeValues: {type: GraphQLString},
  }
});

const DimTagDataResultType = new GraphQLObjectType({
  name: 'DimTagDataResult',
  fields: {
    total: {type: GraphQLInt},
    rows: {type: new GraphQLList(DimTagDataType)},
  }
});

const dimTagDataResolver = ({ input: { tenantId, loginId, appId, tagId, dimDataCodeValues, dimDataParentCodeValues }}) => {
  let result = [];
  if (tagId) {
    result = dimTagData[tagId];
  }
  if (dimDataCodeValues) {
    result = dimTagData[tagId];
  }
  if (dimDataCodeValues) {
    const dimDataCodeValueArray = dimDataCodeValues.split(',');
    result = result.filter(one => (dimDataCodeValueArray.findIndex(code => one.code === code) !== -1));
  }
  if (dimDataParentCodeValues) {
    const dimDataParentCodeValueArray = dimDataParentCodeValues.split(',');
    result = result.filter(one => (dimDataParentCodeValueArray.findIndex(code => one.parentCode === code) !== -1));
  }
  return new QueryResult(result);
};

module.exports = {
  DimTagDataResultType,
  DimTagDataInput,
  dimTagDataResolver
};
