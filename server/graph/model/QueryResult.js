class QueryResult {
  constructor(rows) {
    this.rows = rows;
  }
  total() {
    return this.rows.length;
  }
}

module.exports = QueryResult;
