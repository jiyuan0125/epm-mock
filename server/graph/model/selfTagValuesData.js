const fs = require('fs');
const selftTagValuesData = JSON.parse(fs.readFileSync('data/selfTagValuesData.json'));
const QueryResult = require('./QueryResult');
const {
  GraphQLObjectType,
  GraphQLList,
  GraphQLInt,
  GraphQLString,
  GraphQLInputObjectType,
} = require('graphql');

const SelfTagValuesDataType = new GraphQLObjectType({
  name: 'SelfTagValuesDataType',
  fields: {
    initValue: {type: GraphQLString},
    initDesc: {type: GraphQLString},
    value: {type: GraphQLString},
    desc: {type: GraphQLString},
    ord: {type: GraphQLString},
  },
});

const SelfTagValuesDataInput = new GraphQLInputObjectType({
  name: 'SelfValuesDataInput',
  fields: {
    tenantId: {type: GraphQLString},
    loginId: {type: GraphQLString},
    appId: {type: GraphQLString},
  }
});

const SelfTagValuesDataQueryResultType = new GraphQLObjectType({
  name: 'SelfValuesDataQueryResult',
  fields: {
    total: {type: GraphQLInt},
    rows: {type: new GraphQLList(SelfTagValuesDataType)},
  }
});

const selfTagValuesDataResolver = ({input: {tenantId, loginId, appId}}) => {
  let result = selftTagValuesData;
  return new QueryResult(result);
};

module.exports = {
  SelfValuesDataQueryResultType: SelfTagValuesDataQueryResultType,
  SelfValuesDataInput: SelfTagValuesDataInput,
  selfTagValuesDataResolver: selfTagValuesDataResolver
};
