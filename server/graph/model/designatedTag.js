const fs = require('fs');
const tags = JSON.parse(fs.readFileSync('data/tags.json'));
const QueryResult = require('./QueryResult');
const {
  GraphQLObjectType,
  GraphQLList,
  GraphQLInt,
  GraphQLString,
  GraphQLInputObjectType,
} = require('graphql');

const DesignatedTagType = new GraphQLObjectType({
  name: 'DesignatedTagType',
  fields: {
    tagLibId: {type: GraphQLString},
    tagFirstDirId: {type: GraphQLString},
    tagFirstDirName: {type: GraphQLString},
    tagDirId: {type: GraphQLString},
    tagDirName: {type: GraphQLString},
    tagId: {type: GraphQLString},
    tagName: {type: GraphQLString},
    parentTagId: {type: GraphQLString},
    datasetColumn: {type: GraphQLString},
    tagType: {type: GraphQLString},
    aqlStr: {type: GraphQLString},
    dataType: {type: GraphQLString},
    dataFmt: {type: GraphQLString},
    tagUnit: {type: GraphQLString},
    dimSettingJson: {type: GraphQLString},
    techDesc: {type: GraphQLString},
    busiDesc: {type: GraphQLString},
    postMan: {type: GraphQLString},
    postDept: {type: GraphQLString},
    effecDate: {type: GraphQLString},
    exparDate: {type: GraphQLString},
    tagVersion: {type: GraphQLString},
    isUpState: {type: GraphQLString},
  },
});

const DesignatedTagInput = new GraphQLInputObjectType({
  name: 'DesignatedTagInput',
  fields: {
    tenantId: {type: GraphQLString},
    loginId: {type: GraphQLString},
    appId: {type: GraphQLString},
    tagIds: {type: GraphQLString},
  }
});

const DesignatedTagQueryResultType = new GraphQLObjectType({
  name: 'DesignatedTagLibResult',
  fields: {
    total: {type: GraphQLInt},
    rows: {type: new GraphQLList(DesignatedTagType)},
  }
});

const designatedTagResolver = ({ input: { tenantId, loginId, appId, tagIds }}) => {
  let result = tags;
  if (tagIds) {
    const tagIdArray = tagIds.split(',');
    result = result.filter(one => (tagIdArray.findIndex(id => one.tagId === id) !== -1));
  } else {
    result = [];
  }
  return new QueryResult(result);
};

module.exports = {
  DesignatedTagQueryResultType,
  DesignatedTagInput,
  designatedTagResolver
};
