const fs = require('fs');
const tagLibs = JSON.parse(fs.readFileSync('data/tagLibs.json'));
const QueryResult = require('./QueryResult');
const {
  GraphQLObjectType,
  GraphQLList,
  GraphQLInt,
  GraphQLString,
  GraphQLInputObjectType,
} = require('graphql');

const DesignatedTagLibType = new GraphQLObjectType({
  name: 'DesignatedTagLibType',
  fields: {
    tagLibId: {type: GraphQLString},
    tagLibName: {type: GraphQLString},
    tagLibDesc: {type: GraphQLString},
    datasetId: {type: GraphQLString},
    datasetName: {type: GraphQLString},
    datasetVersion: {type: GraphQLString},
    datasetConditionJson: {type: GraphQLString},
    primaryTagsJson: {type: GraphQLString},
    createTime: {type: GraphQLString},
  },
});

const DesignatedTagLibInput = new GraphQLInputObjectType({
  name: 'DesignatedTagLibInput',
  fields: {
    tenantId: {type: GraphQLString},
    loginId: {type: GraphQLString},
    appId: {type: GraphQLString},
    tagLibIds: {type: GraphQLString},
  }
});

const DesignatedTagLibQueryResultType = new GraphQLObjectType({
  name: 'DesignatedTagLibQueryResult',
  fields: {
    total: {type: GraphQLInt},
    rows: {type: new GraphQLList(DesignatedTagLibType)},
  }
});

const designatedTagLibResolver = ({input: { tenantId, loginId, appId, tagLibIds}}) => {
  let result = tagLibs;
  if (tagLibIds) {
    const tagLibArray = tagLibIds.split(',');
    result = result.filter(one => (tagLibArray.findIndex(id => one.tagLibId === id) !== -1));
  } else {
    result = [];
  }
  return new QueryResult(result);
};

module.exports = {
  DesignatedTagLibInput,
  DesignatedTagLibQueryResultType,
  designatedTagLibResolver,
};
