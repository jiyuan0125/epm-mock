const fs = require('fs');
const tagLibs = JSON.parse(fs.readFileSync('data/tagLibs.json'));
const QueryResult = require('./QueryResult');

const {
  GraphQLObjectType,
  GraphQLList,
  GraphQLInt,
  GraphQLString,
  GraphQLInputObjectType,
} = require('graphql');

const TagLibType = new GraphQLObjectType({
  name: 'TagLibType',
  fields: {
    tagLibId: {type: GraphQLString},
    tagLibName: {type: GraphQLString},
    tagLibDesc: {type: GraphQLString},
    datasetId: {type: GraphQLString},
    datasetName: {type: GraphQLString},
    datasetVersion: {type: GraphQLString},
    datasetConditionJson: {type: GraphQLString},
    createTime: {type: GraphQLString},
  },
});

const TagLibInput = new GraphQLInputObjectType({
  name: 'TagLibInput',
  fields: {
    tenantId: {type: GraphQLString},
    loginId: {type: GraphQLString},
    appId: {type: GraphQLString},
    tagLibId: {type: GraphQLString},
  }
});

const TagLibQueryResultType = new GraphQLObjectType({
  name: 'TagLibQueryResult',
  fields: {
    total: {type: GraphQLInt},
    rows: {type: new GraphQLList(TagLibType)},
  }
});

const tagLibResolver = ({input: { tenantId, loginId, appId, tagLibId}}) => {
  let result = tagLibs;
  if (tagLibId) {
    result = result.filter(one => one.tagLibId === tagLibId);
  }
  return new QueryResult(result);
};

module.exports = {
  TagLibInput,
  TagLibQueryResultType,
  tagLibResolver
};
