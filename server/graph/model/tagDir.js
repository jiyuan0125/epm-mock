const fs = require('fs');
const tagDirs = JSON.parse(fs.readFileSync('data/tagDirs.json'));
const QueryResult = require('./QueryResult');
const {
  GraphQLObjectType,
  GraphQLList,
  GraphQLInt,
  GraphQLString,
  GraphQLInputObjectType,
} = require('graphql');

const TagDirType = new GraphQLObjectType({
  name: 'TagDirType',
  fields: {
    tagLibId: {type: GraphQLString},
    tagDirId: {type: GraphQLString},
    tagDirName: {type: GraphQLString},
    tagDirParentId: {type: GraphQLString},
    tagDirOrd: {type: GraphQLString},
    tagDirPath: {type: GraphQLString},
    isLeaf: {type: GraphQLInt},
    createTime: {type: GraphQLString},
  },
});

const TagDirQueryResultType = new GraphQLObjectType({
  name: 'TagDirResult',
  fields: {
    total: {type: GraphQLInt},
    rows: {type: new GraphQLList(TagDirType)},
  }
});

const TagDirInput = new GraphQLInputObjectType({
  name: 'TagDirInput',
  fields: {
    tenantId: {type: GraphQLString},
    loginId: {type: GraphQLString},
    appId: {type: GraphQLString},
    tagLibId: {type: GraphQLString},
    tagDirId: {type: GraphQLString},
    tagDirName: {type: GraphQLString},
    withAncestors: {type: GraphQLString},
  }
});

const recursiveFindParentDir = (dirs, dir, set) => {
  const parent = dirs.find(one => one.tagDirId === dir.tagDirParentId);
  if (parent) {
    set.add(parent);
    recursiveFindParentDir(dirs, parent, set);
  }
};

const tagDirResolver = ({input: { tenantId, loginId, appId, tagLibId, tagDirId, tagDirName, withAncestors}}) => {
  let result = tagDirs;
  if (tagLibId) {
    result = result.filter(one => one.tagLibId === tagLibId);
  }
  if (tagDirId) {
    result = result.filter(one => one.tagDirId === tagDirId);
  }
  if (tagDirName) {
    result = result.filter(one => one.tagDirName === tagDirName);
  }

  if (withAncestors === '1') {
    const set = new Set();
    result.forEach(one => {
      set.add(one);
      recursiveFindParentDir(tagDirs, one, set);
    });
    result = Array.from(set);
  }

  return new QueryResult(result);
};


module.exports = {
  TagDirQueryResultType,
  TagDirInput,
  tagDirResolver
};
