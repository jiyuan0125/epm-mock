const fs = require('fs');
const {
  GraphQLObjectType,
  GraphQLSchema,
} = require('graphql');

const {TagLibQueryResultType, TagLibInput, tagLibResolver} = require('./tagLib');
const {DesignatedTagLibQueryResultType, DesignatedTagLibInput, designatedTagLibResolver} = require('./designatedTagLib');
const {TagDirQueryResultType, TagDirInput, tagDirResolver} = require('./tagDir');
const {TagQueryResultType, TagInput, tagResolver} = require('./tag');
const {DesignatedTagQueryResultType, DesignatedTagInput, designatedTagResolver} = require('./designatedTag');
const {SelfValuesDataQueryResultType, SelfValuesDataInput, selfTagValuesDataResolver} = require('./selfTagValuesData');
const {DimTagDataResultType, DimTagDataInput, dimTagDataResolver} = require('./dimTagData');

// --- Getmodel
const GetmodelType = new GraphQLObjectType({
  name: 'GetmodelType',
  fields: {
    tagLibs: {
      type: TagLibQueryResultType,
      args: {
        input: {type: TagLibInput}
      },
    },
    designatedTagLibs: {
      type: DesignatedTagLibQueryResultType,
      args: {
        input: {type: DesignatedTagLibInput}
      },
    },
    designatedTag: {
      type: DesignatedTagQueryResultType,
      args: {
        input: {type: DesignatedTagInput}
      },
    },
    tagDirs: {
      type: TagDirQueryResultType,
      args: {
        input: {type: TagDirInput}
      },
    },
    tags: {
      type: TagQueryResultType,
      args: {
        input: {type: TagInput}
      },
    },
    selfTagValuesData: {
      type: SelfValuesDataQueryResultType,
      args: {
        input: {type: SelfValuesDataInput}
      },
    },
    dimTagData: {
      type: DimTagDataResultType,
      args: {
        input: {type: DimTagDataInput}
      },
    }
  }
});

// --- Query
const QueryType = new GraphQLObjectType({
  name: 'Query',
  fields: {
    getmodel: {
      type: GetmodelType,
      resolve: () => {
        return {
          tagLibs: tagLibResolver,
          designatedTagLibs: designatedTagLibResolver,
          tagDirs: tagDirResolver,
          tags: tagResolver,
          designatedTag: designatedTagResolver,
          selfTagValuesData: selfTagValuesDataResolver,
          dimTagData: dimTagDataResolver,
        };
      }
    },
  }
});

const schema = new GraphQLSchema({query: QueryType});

module.exports = schema;
