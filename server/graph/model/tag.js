const fs = require('fs');
const tags = JSON.parse(fs.readFileSync('data/tags.json'));
const QueryResult = require('./QueryResult');
const {
  GraphQLObjectType,
  GraphQLList,
  GraphQLInt,
  GraphQLString,
  GraphQLInputObjectType,
} = require('graphql');

const TagType = new GraphQLObjectType({
  name: 'TagType',
  fields: {
    tagLibId: {type: GraphQLString},
    tagFirstDirId: {type: GraphQLString},
    tagFirstDirName: {type: GraphQLString},
    tagDirId: {type: GraphQLString},
    tagDirName: {type: GraphQLString},
    tagId: {type: GraphQLString},
    tagName: {type: GraphQLString},
    parentTagId: {type: GraphQLString},
    datasetColumn: {type: GraphQLString},
    tagType: {type: GraphQLString},
    aqlStr: {type: GraphQLString},
    dataType: {type: GraphQLString},
    dataFmt: {type: GraphQLString},
    tagUnit: {type: GraphQLString},
    dimSettingJson: {type: GraphQLString},
    techDesc: {type: GraphQLString},
    busiDesc: {type: GraphQLString},
    postMan: {type: GraphQLString},
    postDept: {type: GraphQLString},
    effecDate: {type: GraphQLString},
    exparDate: {type: GraphQLString},
    childrenTagIdsJson: {type: GraphQLString},
    hasAuthSetting: {type: GraphQLString},
    authSettingValue: {type: GraphQLString},
    tagSampleData: {type: GraphQLString},
  },
});

const TagQueryResultType = new GraphQLObjectType({
  name: 'TagResult',
  fields: {
    total: {type: GraphQLInt},
    rows: {type: new GraphQLList(TagType)},
  }
});

const TagInput = new GraphQLInputObjectType({
  name: 'TagInput',
  fields: {
    tenantId: {type: GraphQLString},
    loginId: {type: GraphQLString},
    appId: {type: GraphQLString},
    tagLibId: {type: GraphQLString},
    tagId: {type: GraphQLString},
    tagName: {type: GraphQLString},
    hasAuthSetting: {type: GraphQLString},
  }
});

const tagResolver = ({ input: { tenantId, loginId, appId, tagLibId, tagId, tagName, hasAuthSetting }}) => {
  let result = tags;
  if (tagLibId) {
    result = result.filter(one => one.tagLibId === tagLibId);
  }
  if (tagId) {
    result = result.filter(one => one.tagId === tagId);
  }
  if (tagName) {
    result = result.filter(one => one.tagName === tagLibId);
  }
  if (hasAuthSetting) {
    result = result.filter(one => one.hasAuthSetting === hasAuthSetting);
  }
  return new QueryResult(result);
};

module.exports = {
  TagQueryResultType,
  TagInput,
  tagResolver,
};
