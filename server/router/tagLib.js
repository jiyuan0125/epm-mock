const express = require('express');
const router = express.Router();

const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

router.post('/', jsonParser, (req, res) => {
  res.json({
    result: 'true',
    message: '成功',
  });
});

module.exports = router;
