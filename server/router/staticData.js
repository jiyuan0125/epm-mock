module.exports = function (req) {
  if (req.body.aql.indexOf("dbm_20201030_ee2c18df") !== -1) {
    console.log(req.body.aql);
    console.log(/\((['\w',\s]+)\)/.exec(req.body.aql));
    const tmp = /\((['\w',\s]+)\)/.exec(req.body.aql)[1].split(/,\s*/);
    result = [];
    tmp.forEach((it) => {
      const user_id = it.replace(/'/g, "");
      result.push({
        user_id,
        serial_number: "serial_number_" + user_id,
      });
    });
    return result;
  }
  if (req.body.aql.indexOf("dbm_20201104_faed9626") !== -1) {
    console.log(req.body.aql);
    console.log(/\((['\w',\s]+)\)/.exec(req.body.aql)[1]);
    const tmp = /\((['\w',\s]+)\)/.exec(req.body.aql)[1].split(/,\s*/);
    result = [];
    tmp.forEach((it) => {
      const user_id = it.replace(/'/g, "");
      result.push({
        user_id,
        product_id_wd: "90356341",
      });
    });
    return result;
  }

  return null;
};
