const express = require("express");
const staticData = require("./staticData.js");
const router = express.Router();

const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();

const { numOfPages } = require("../../config");

const getColumns = (select) => {
  const columns = select[1].split(",");

  // 检查是否带有"as"
  const regexp = /as\s+?(\S+)/i;
  for (let i = 0; i < columns.length; i++) {
    const match = columns[i].match(regexp);
    if (match) {
      columns[i] = match[1];
    }
  }

  // 如果含有"."符号，只保留"."之后的列名
  for (let i = 0; i < columns.length; i++) {
    const tmpColumNameArray = columns[i].split(".");
    if (tmpColumNameArray.length > 1) {
      columns[i] = tmpColumNameArray[1];
    }
  }
  return columns;
};

const getHasLimit = (aql) => {
  return /limit/i.test(aql);
};

const getPageSize = (aql) => {
  const splitArray = aql.split(/limit/i);
  const limitToken = splitArray[splitArray.length - 1];
  return parseInt(limitToken.split(",")[1]);
};

const getRow = (columns) => {
  const row = {};
  columns.forEach((column) => {
    row[column] = Math.floor(Math.random() * 1000);
  });
  return row;
};

const createData = ({ aql }, staticData) => {
  if (!aql) return {};
  const select = aql.match(/select\s+(.*)\s+from/i);
  if (!select) return {};

  const columns = getColumns(select);
  const hasLimit = getHasLimit(aql);

  if (!hasLimit) {
    if (staticData) {
      return staticData;
    } else {
      return getRow(columns);
    }
  } else {
    const pageSize = getPageSize(aql);
    const total = pageSize * numOfPages;
    if (staticData) {
      return {
        total,
        rows: staticData,
      };
    } else {
      const rows = [];
      for (let i = 0; i < pageSize; i++) {
        rows[i] = getRow(columns);
      }

      return {
        total,
        rows,
      };
    }
  }
};

router.post("/api/query/data/v1", jsonParser, (req, res) => {
  const data = createData(req.body, staticData(req));
  const responseData = {
    result: true,
    data: JSON.stringify(data),
  };
  console.log(responseData);
  //res.json(responseData);
  setTimeout(() => {
    res.json(responseData);
  }, 10000);
});

module.exports = router;
