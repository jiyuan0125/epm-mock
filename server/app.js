const express = require("express");
const path = require("path");
const mustacheExpress = require("mustache-express");
const { port } = require("../config");

const index = require("./router/index");
const databroker = require("./router/databroker");
const tagLib = require("./router/tagLib");

const graphqlHTTP = require("express-graphql");
const schema = require("./graph/model/schema");

const app = express();
app.engine("mustache", mustacheExpress());

app.set("view engine", "mustache");
app.set("view cache", false);
app.set("views", path.join(__dirname, "/views"));

app.use(
  "/model",
  graphqlHTTP({
    schema,
    rootValue: "root",
    graphiql: true,
  })
);

app.use("/", index);
app.use("/databroker", databroker);
app.use("/api/recordUseTags", tagLib);

app.listen(port, () => console.log(`Mock app is listening on port ${port}!`));
