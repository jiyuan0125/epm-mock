const request = require('./request');
const config = require('../config');

const query = `
query SelfTagValuesData($tenantId: String!, $loginId: String!, $appId: String!) {
  getmodel {
    selfTagValuesData(input: {tenantId: $tenantId, loginId: $loginId, appId: $appId}) {
      total
      rows {
        initValue
        initDesc
        value
        desc
        ord
      }
    }
  }
}`;

const getSelfTagValuesData = async (variables) => {
  try {
    const result = await request.post(config.serverAddress,
      {query, variables: {
          ...config.commonVariables,
          ...variables,
        }},
    );
    if (result.data.data) {
      return result.data.data.getmodel.selfTagValuesData.rows;
    } else {
      throw new Error(result.data.errors);
    }
  } catch (e) {
    console.log({
      name: e.name,
      message: e.message,
    })
  }

  return [];
};


module.exports = getSelfTagValuesData;
