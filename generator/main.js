const fs = require('fs');
const path = require('path');

const getTagLibs = require('./getTagLibs');
const getTagDirs = require('./getTagDirs');
const getTags = require('./getTags');
const getSelfTagValuesData = require('./getSelfTagValuesData');
const getDimTagData = require('./getDimTagData');

const tagLibsFile = '../data/tagLibs.json';
const tagDirsFile = '../data/tagDirs.json';
const tagsFile = '../data/tags.json';
const selfTagValuesDataFile = '../data/selfTagValuesData.json';
const dimTagDataFile = '../data/dimTagData.json';

const clean = () => {
  try {
    fs.unlinkSync(tagLibsFile);
    fs.unlinkSync(tagDirsFile);
    fs.unlinkSync(tagsFile);
    fs.unlinkSync(selfTagValuesDataFile);
    fs.unlinkSync(dimTagDataFile);
  } catch (e) {
    // Ignore
  }
};

const writeFile = (file, data) => {
  fs.appendFileSync(path.join(__dirname, file), JSON.stringify(data, null, 2), {
    flag: 'a',
  });
};

(async () => {
  clean();
  const tagLibs = await getTagLibs();
  writeFile(tagLibsFile, tagLibs);

  const selfTagValuesData = await getSelfTagValuesData();
  writeFile(selfTagValuesDataFile, selfTagValuesData);

  let tagDirsResult = [];
  let tagsResult = [];
  for (const tagLib of tagLibs) {
    const tagDirs = await getTagDirs({
      tagLibId: tagLib.tagLibId,
    });
    const tags = await getTags({
      tagLibId: tagLib.tagLibId
    });
    tagDirsResult = tagDirsResult.concat(tagDirs);
    tagsResult = tagsResult.concat(tags);
  }

  writeFile(tagDirsFile, tagDirsResult);
  writeFile(tagsFile, tagsResult);

  const dimTagData = {};
  for (const tag of tagsResult) {
    if (tag.tagType === 'dim') {
      dimTagData[tag.tagId] = await getDimTagData({
        tagId: tag.tagId
      });
    }
  }
  writeFile(dimTagDataFile, dimTagData);

})();

