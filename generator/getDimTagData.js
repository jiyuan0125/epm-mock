const request = require('./request');
const config = require('../config');

const query = `
query DimTagData($tenantId: String!, $loginId: String!, $appId: String!, $tagId: String!, $dimDataCodeValues: String, $dimDataParentCodeValues: String) {
  getmodel {
    dimTagData(input: {tenantId: $tenantId, loginId: $loginId, appId: $appId, tagId: $tagId, dimDataCodeValues: $dimDataCodeValues, dimDataParentCodeValues: $dimDataParentCodeValues}) {
      total
      rows {
        code
        name
        parentCode
        ord
      }
    }
  }
}`;

const getDimTagData = async (variables) => {
  try {
    const result = await request.post(config.serverAddress,
      {
        query, variables: {
          ...config.commonVariables,
          ...variables,
        }
      },
    );
    if (result.data.data) {
      return result.data.data.getmodel.dimTagData.rows;
    } else {
      throw new Error(result.data.errors);
    }
  } catch (e) {
    console.log({
      name: e.name,
      message: e.message,
    })
  }

  return [];
};


module.exports = getDimTagData;
