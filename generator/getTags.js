const request = require('./request');
const config = require('../config');

const query = `
query Tags($tenantId: String!, $loginId: String!, $appId: String!, $tagLibId: String) {
  getmodel {
    tags(input: {tenantId: $tenantId, loginId: $loginId, appId: $appId, tagLibId: $tagLibId}) {
      total
      rows {
        tagLibId
        tagFirstDirId
        tagFirstDirName
        tagDirId
        tagDirName
        tagId
        tagName
        parentTagId
        datasetColumn
        tagType
        aqlStr
        dataType
        dataFmt
        tagUnit
        dimSettingJson
        techDesc
        busiDesc
        postMan
        postDept
        effecDate
        exparDate
        childrenTagIdsJson
        hasAuthSetting
        authSettingValue
        tagSampleData
        tagVersion
        isUpState
      }
    }
  }
}`;

const getTags = async (variables) => {
  try {
    const result = await request.post(config.serverAddress,
      {
        query, variables: {
          ...config.commonVariables,
          ...variables,
        }
      },
    );
    if (result.data.data) {
      return result.data.data.getmodel.tags.rows;
    } else {
      throw new Error(result.data.errors);
    }
  } catch (e) {
    // throw e;
    console.log({
      name: e.name,
      message: e.message,
    })
  }

  return [];
};


module.exports = getTags;
