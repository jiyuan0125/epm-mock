const request = require('./request');
const config = require('../config');

const query = `
query TagLibs($tenantId: String!, $loginId: String!, $appId: String!) {
  getmodel {
    tagLibs(input: {tenantId: $tenantId, loginId: $loginId, appId: $appId}) {
      total
      rows {
        tagLibId
        tagLibName
        tagLibDesc
        datasetId
        datasetName
        datasetVersion
        datasetConditionJson
        createTime
        primaryTagsJson
        createTime
      }
    }
  }
}`;

const getTagLibs = async (variables) => {
  try {
    const result = await request.post(config.serverAddress,
      {
        query, variables: {
          ...config.commonVariables,
          ...variables,
        }
      },
    );
    if (result.data.data) {
      return result.data.data.getmodel.tagLibs.rows;
    } else {
      throw new Error(result.data.errors);
    }
  } catch (e) {
    console.log({
      name: e.name,
      message: e.message,
    })
  }

  return [];
};


module.exports = getTagLibs;
