const request = require('./request');
const config = require('../config');

const query = `
query TagDirs($tenantId: String!, $loginId: String!, $appId: String!, $tagLibId: String) {
  getmodel {
    tagDirs(input: {tenantId: $tenantId, loginId: $loginId, appId: $appId, tagLibId: $tagLibId}) {
      total
      rows {
        tagLibId
        tagDirId
        tagDirName
        tagDirParentId
        tagDirOrd
        tagDirPath
        isLeaf
        createTime
      }
    }
  }
}`;

const getTagDirs = async (variables) => {
  try {
    const result = await request.post(config.serverAddress,
      {query, variables: {
          ...config.commonVariables,
          ...variables,
        }},
    );
    if (result.data.data) {
      return result.data.data.getmodel.tagDirs.rows;
    } else {
      throw new Error(result.data.errors);
    }
  } catch (e) {
    console.log({
      name: e.name,
      message: e.message,
    })
  }

  return [];
};


module.exports = getTagDirs;
