const axios = require('axios');
axios.defaults.headers.post['Content-Type'] = 'application/json';

module.exports = axios;
