# MOCK 标签库与数据集

## 标签库访问地址
http://localhost:5998/model

## 数据集访问地址
http://localhost:5998/databroker/

## 运行方法
```bash
npm install
npm start
```

## 重新生成MOCK数据
1. 修改 config.js 里的 serverAddress，指向真实的标签库地址
2. 运行
```bash
npm run gen
```
